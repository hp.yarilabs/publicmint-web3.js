# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- EXAMPLE

## [1.0.0]

### Added

- I've added feature XY (#1000)

### Changed

- I've cleaned up XY (#1000)

### Deprecated

- I've deprecated XY (#1000)

### Removed

- I've removed XY (#1000)

### Fixed

- I've fixed XY (#1000)

### Security

- I've improved the security in XY (#1000)

-->

## [1.0.0]

Released as testing code base.

## [1.0.1]

Released with 1.0.0 code base from private repo.

### Fixed

- Providers info
- Configuration for contracts
- Babel and webpack configuration
- Export of web3.utils.sha3 as web3.pm.utils.sha256 
- Throw when provider is invalid
- auto estimateGas when not use gas param or value lower than 0
  
### Added

- Auto estimateGas for <contract-name>.contract 
- EstimatedCosts method for withdrawals methods in ERC20 tokens
- Tutorials
- Documentation
- Examples by feature of user
- New endpoints of providers for RPC
- Logo
- Query module
- TransactionHash validation in utils module
