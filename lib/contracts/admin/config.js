/**
 * MultiSignature configuration file
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Contract name (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name: 
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 * @property {Array.<Object.<chainId, address>>}  adminsTo - Info with address and chainId
 * 
 * @property {string}  target - MultiSig target
 * @property {Array.string}  proxyMethods - MultiSig target methods to call
 */
export default [{
  "name": "USD",
  "abi": "GNOSIS",
  "deployType": "genesis",
  "deployInfo": [{
    "address": "0x0000000000000000000000000000000000001010"
  }],
  "adminsTo": [{
    "target": "token.USD",
    "proxyMethods": ["deposit"]
  }]
}, {
  "name": "GasManager",
  "abi": "GNOSIS",
  "deployType": "genesis",
  "deployInfo": [{
    "address": "0x0000000000000000000000000000000000001020"
  }],
  "adminsTo": [{
    "target": "operations.GasManager",
    "proxyMethods": ["addContractWhitelist", "rmContractWhitelisted", "setGasPrice", "addERC", "rmERC", "addFee", "enableFee", "disableFee", "updateFeeAmount", "updateFeeTopic", "transferTokens"]
  }]
}];
//# sourceMappingURL=config.js.map