/**
 * Module Utils
 */
import Conversions from './Conversions';
export default function (web3In) {
  return { ...Conversions(web3In),

    /** 
     * sha256
     * To mimic the sha3 behaviour of solidity use soliditySha3
     * @memberof pm.utils
     * @param { String } - A String to hash
     * @return { String } - The result hash
     */
    sha256: key => {
      return web3In.sha3(key);
    },

    /** 
     * isTransactionHash
     * Check if string is a transaction hash 
     * @memberof pm.utils
     * @param { String } - A String to hash
     * @return { Boolean } - Return true if is transactionHash otherwise false 
     */
    isTransactionHash: transactionHash => {
      const pattern = /0x([A-Fa-f0-9]{64})/;
      return pattern.test(transactionHash);
    }
  };
}
//# sourceMappingURL=index.js.map