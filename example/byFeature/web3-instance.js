const PublicMint = require('../../dist/index.js').default;

console.log('Modules info: ', PublicMint.versions());

// default provider is 'ws'
module.exports = new PublicMint(2019, "http");

// Provider 2019 is testNet
// Provider 2020 is MainNet
