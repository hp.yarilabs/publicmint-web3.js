// Load web3 instance with wallet created
const web3 = require('./wallet');

const {
    wallet
} = web3.pm;

const {
    USD
} = web3.pm.contracts.token;

const {
    fromToken,
    toToken,
    sha256
} = web3.pm.utils

function log(...args) {
    console.info('\n', args);
}

const from = accounts[0];

(async function () {
    // GET ALL NATIVE BALANCES
    const balances = await wallet.accounts.getBalances;
    log('\n All balances from wallet: ', balances);

    // *** WARING - LOST OF TOKENS *** DON'T GENERATE REF FROM API BY YOUR SELF, GET CORRECT REFERENCE FROM PUBLIC MINT API
    const refFromApi = sha256(`{id: ${+new Date()}, desc: "Just a example of deposit ref api for " + ${recipientAccount} }`);


    // Withdrawals available:
    // withdrawWireUS
    // withdrawWireInt
    // withdrawAchInt


    // See all cost associated by each method:
    // await USD.withdrawWireInt(value, ref).estimateCosts();
    // output
    /*
        { 
            gas: '26108',
            gasPrice: '20000000000',
            additionalFee: '50000000000000000000',
            totalCost: '50000522160000000000' 
        }
    */
    const receipt = await USD.withdrawWireUS(toToken('1'), refFromApi).send({
        from
    });

    log("\n Receipt", receipt)

    process.exit(0);
})();
