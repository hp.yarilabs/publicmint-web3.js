const web3 = require('./web3-instance');

const {
    wallet
} = web3.pm;

const walletAccountPrivateKey1 =
    '0xba6cdfcc795484a9774eb98e756983da822ef3481b37d4ba649864e2d1ab4e5e';

wallet.add(walletAccountPrivateKey1);

module.exports = web3;
