// Load web3 instance with wallet created
const web3 = require('./wallet');

const {
    wallet
} = web3.pm;

const {
    USD
} = web3.pm.contracts.token;

function log(...args) {
    console.info('\n', args);
}

const accounts = wallet.accounts.getAccounts;
log('\n Accounts', accounts);

const from = accounts[0];


(async function () {
    // GET ALL NATIVE BALANCES
    const balances = await wallet.accounts.getBalances;
    log('\n All balances from wallet: ', balances);

    // GET SINGLE ACCOUNT BALANCE
    const balanceAccount1 = await wallet.accounts.getBalance(from);
    log('\n Single balance from wallet: ', balanceAccount1);

    // More slow and heavy for blockchain, prefer using the above options
    // ERC20 INTERACTION
    const myBalance = await USD.balanceOf(from).call();
    log('\n myBalance', myBalance);

    process.exit(0);
})();
