// Load web3 instance with wallet created
const web3 = require('./wallet');

const {
    wallet
} = web3.pm;

function log(...args) {
    console.info('\n', args);
}

// List accounts in wallet
const accounts = wallet.accounts.getAccounts;
log('\n Accounts', accounts);


const from = accounts[0];

(async function () {
    // TRANSFER TOKENS
    const recipientAccount = '0xCa1BA9A1f19A3635af8A561bD75840Cef2Dc3dfC';
    const value = '2000000000000000000'; // wei value

    const receiptTransfer = await wallet.accounts.transfer(recipientAccount, value);
    log('\n Receipt of transfer: ', receiptTransfer);

    // transferFrom(<index wallet address> || address, to address, value in wei)
    // await wallet.accounts.transferFrom(0,recipientAccount, value);
    process.exit(0);
})();
