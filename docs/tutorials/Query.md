
## Query Module 

Utils for query transaction data and resolve information

## Examples

```js

import PublicMint from '@publicmint/publicmint-web3';

const web3 = new PublicMint(2018); // devNet

const query = web3.pm.query;

const {
    USD
} = web3.pm.contracts.token


// TransactionHash of withdrawal
const hash = "0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915b32";

const result = await query.getWithdrawalTypeByContract(USD, hash);
// Returns: 'withdrawWireUS'

// can pass USD contract instance or USD address
const result = await query.getWithdrawalTxByContract(USD, hash);
/*
Returns: 
{ type: 'withdrawWireUS',
  decodedValue:
   Result {
     '0': '1',
     '1':
      '0x681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a',
     __length__: 2,
     amount: '1',
     ref:
      '0x681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a' },
  transaction:
   { blockHash:
      '0x205545eb5e8430cd0958a67b26d947f6025536c59759b3f31cc248d1f725be0e',
     blockNumber: 21302,
     from: '0xe1650dD7C9Aef65264341d2c16f0C9D3348723bb',
     gas: 66942,
     gasPrice: '20000000000',
     hash:
      '0x75399824572df192c684765802a85f408f8b8ae1f9939371efecb0267e915b32',
     input:
      '0xda2eaba00000000000000000000000000000000000000000000000000000000000000001681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a',
     nonce: 5,
     to: '0x0000000000000000000000000000000000002070',
     transactionIndex: 0,
     value: '0',
     v: '0xfe7',
     r:
      '0x3ae6a4692a174f47f9b2042cdce8f7c93c8b8d2530f215626dfe76089d32b41',
     s:
      '0x1341de06e24a1377bd0aab2b916ec59c7b0d579b619f7c1959f05b52e756771d' },
  abi:
   { constant: false,
     inputs: [ [Object], [Object] ],
     name: 'withdrawWireUS',
     outputs: [ [Object] ],
     payable: false,
     stateMutability: 'nonpayable',
     type: 'function',
     signature: '0xda2eaba0' } }
*/

```
