## Conversions utils

Abstration methods for convert tokens units, same as fromWei, toWei [web3-utils](https://web3js.readthedocs.io/en/v1.2.4/web3-utils.html#fromwei).

Public Mint blockchain use same units like ethereum [web3-utils](https://web3js.readthedocs.io/en/v1.2.4/web3-utils.html#id76).


### Requirements

```js

// using static module same as above
const utils = PublicMint.utils;

// OR 
// web3 = new PublicMint();
// utils = web3.pm.utils;

const {
    fromToken,
    toToken
} = utils

```

### fromToken 
- Converts small token unit with 18 decimals for USD unit

```js
fromToken('1000000000000000000') === '1'
```

### toToken 
- Converts USD to token unit

```js
toToken('1') === '1000000000000000000'
```
