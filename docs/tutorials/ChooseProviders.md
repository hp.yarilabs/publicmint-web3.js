
## Preconfigured providers 

All preconfigured ids [here]{@link ProviderConfigurationFiles}

## Example using testing network

It's possible to pass a id or name of network ([provider options]{@link PublicMint}).

```js

// Require package
const PublicMint = require('@publicmint/publicmint-web3')

// mainNet is a default provider 
const web3 = new PublicMint('testNet')

```
List of providers:
- localNet (e.g. local running PublicMint client)
- devNet, id: 2018  (only PublicMint team)
- testNet, id: 2019 (all users)
- mainNet, id: 2020 (default)

Other networks: 
- ganache (PublicMint contract will not work) only if --fork is defined [ganache-cli doc](https://github.com/trufflesuite/ganache-cli/blob/master/README.md)


[Add custom provider]{@AddCustomProvider}
