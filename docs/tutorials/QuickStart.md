
## Documentation Resources

PublicMint require web3 as dependency, you can find more info about other modules in the link bellow.
- [Web3](https://web3js.readthedocs.io/en/v1.2.4/web3.html)


## Init node project 

See npm docs [npm-init](https://docs.npmjs.com/cli/init)

## Install in your project

```bash

npm install @publicmint/publicmint-web3

```

## Examples

[see repo examples](https://gitlab.com/public-mint-community/publicmint-web3.js/-/blob/master/example/single-file.js)

Default network is 2020 - 'mainNet'

```js

import PublicMint from '@publicmint/publicmint-web3';

// Create web3 umbrella class with PublicMint namespace `pm`
// By default is "ws" and <2020> - <mainNet>, only 'ws' provider it's possible listen events.
const web3 = new PublicMint();

// For <testNet> or chainID <2019> and http service provider see example bellow
// const web3 = new PublicMint(2019, "http")

```

About loaded modules: 
- All modules from web3 are loaded excluding eth.personal, eth.ens, bzz, shh [web3 modules](https://web3js.readthedocs.io/en/v1.2.4/web3.html#returns);
- See more available modules of [pm]{@link pm};

## Run

```bash

node index.js

```

# Accounts actions
- [USD]{@tutorial USD}
- [Accounts methods]{@tutorial AccountMethods}

