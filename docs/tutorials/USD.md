# USD 

Using USD contract interface this is based on ERC-20 Token Standard, for transfer tokens 
[see this interface]{@tutorial AccountMethods}

> IMPORTANT: Add wallet keys [see more]{@tutorial AddWalletKeys}

### Requirements
```js

const {
    toToken, sha256
} =  web3.pm.utils;

const {
	USD
} = web3.pm.contracts.token

```

#### balanceOf
- See balance of account (this not need wallet private keys)
```js
const myBalance = await USD.balanceOf('<address here>').call();
```
> Prefer using [web3.pm.wallet.accounts.getBalance]{@link Accounts} or [web3.eth.getBalance](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#getbalance) for performance reasons

> ! The PublicMint api will give a correct `referenceFromApi` parameter to withdrawal tokens don't generate by your self.

##### Withdrawals


###### Withdraw Int

- Generic passing type of withdraw
```js
// withdraw 1 USD this is eq to '1000000000000000000' tokens
const tokensToWithdraw = toToken('1');

// API from Public Mint will provide a reference for each withdraw for your account
const referenceFromApi = sha256('REF FROM API ');
const withdrawType = sha256('ACH');

await USD.withdrawWireInt(tokensToWithdraw, referenceFromApi, withdrawType).send({
    from: "<Wallet address here>",
});
```


###### Withdraw Wire US
- Withdraw for Wire method
```js
await USD.withdrawWireUS(tokensToWithdraw, referenceFromApi).send({
    from: "<Wallet address here>",
});
```

###### Withdraw ACH
- Withdraw for ACH method
```js
await USD.withdrawAchUS(tokensToWithdraw, referenceFromApi).send({
    from: "<Wallet address here>",
});
```


[See more]{@tutorial Withdrawals} of Withdrawal costs
