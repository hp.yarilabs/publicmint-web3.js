## Withdrawals

!!! NOTE !!!
This is just an example of value, for get the actual costs you need to invoke each existing method with `estimateCost()`.

### Example for ERC20.USD


```js
 await USD.withdrawAchUS(value, ref).estimateCosts();

// output
/*
{
  gas: '26043',
  gasPrice: '20000000000',
  additionalFee: '5000000000000000000',
  totalCost: '5000520860000000000'
}
*/

```


```js
 await USD.withdrawWireInt(value, ref).estimateCosts();

// output
/*
{ gas: '26108',
  gasPrice: '20000000000',
  additionalFee: '50000000000000000000',
  totalCost: '50000522160000000000' 
}
*/

```


```js
 await USD.withdrawWireUS(value, ref).estimateCosts();

// output
/*
{ gas: '26130',
  gasPrice: '20000000000',
  additionalFee: '20000000000000000000',
  totalCost: '20000522600000000000'
}
*/

```


[See more of estimateGas]{@tutorial EstimateGas}


