# Hierarchical Deterministic Wallet – HD Wallet 

This function allow recovery accounts in wallet from mnemonic phrases

```js 

const web3 = new PublicMint()

const {
	wallet
} = web3.pm


const mnemonic = 'cook blur blur frown food potato again grain unfold level stage connect';


// arguments <seed of words>, start index, total number of addresses to import
const privKeys = web3.pm.wallet.importFromSeed(mnemonic, 0, 2);
/*
 Returns the imported private keys: 
 [ 
  '0xba6cdfcc795484a9774eb98e756983da822ef3481b37d4ba649864e2d1ab4e5e',
  '0xccae15162e106a9fe8b05e5bf6d758041daf1d8f25edae61f2ba392a774183c8' 
  ]
*/

const walletAccounts = web3.pm.wallet.accounts.getAccounts;
/*
[
  '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd',
  '0xd2FA48924906e00069E81aaf778c4f586b5CA58B'
]
*/

```
