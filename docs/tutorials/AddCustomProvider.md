
## Providers class 

See methods [here]{@link Provider}

## Example using testing network

It's possible pass a id or name of network ([provider options]{@link PublicMint}).

```js

const customProvider = PublicMint.provider(5, "CustomNetwork", "http://127.0.0.1:7545/", "ws://127.0.0.1:7545/", "http://127.0.0.1/");

const web3 = new PublicMint(customProvider)

const providerSetting = new PublicMint(customProvider).pm.provider;

```
