## Estimate Gas and Costs

### Example for ERC20.USD

All smart contracts calls that change state will cost gas, is the case for methods with [.send()](https://web3js.readthedocs.io/en/v1.2.5/web3-eth-contract.html#methods-mymethod-send).

In view methods is used [.call()](https://web3js.readthedocs.io/en/v1.2.5/web3-eth-contract.html#methods-mymethod-call), this methods not create any transaction is just for reading state of smart contract.


For methods .send() we can calculate the cost in tokens of transaction calling myMethod.estimateGas [estimateGas](https://web3js.readthedocs.io/en/v1.2.5/web3-eth-contract.html#methods-mymethod-estimategas) and [eth.gasPrice()](https://web3js.readthedocs.io/en/v1.2.5/web3-eth.html#getgasprice), the total cost in tokens is: 
- CostInTokens = gasPrice * estimatedGas.
  
This is just a estimate, the real cost can oscillate values at 10% of final the value.


Example: 
```js 
var TestContract = require("./Test.sol");

// getGasPrice returns the gas price on the current network
TestContract.web3.eth.getGasPrice(function(error, result){ 
    var gasPrice = Number(result);
    console.log("Gas Price is " + gasPrice + " wei"); // "10000000000000"

    // Get Contract instance
    TestContract.deployed().then(function(instance) {

        // Use the keyword 'estimateGas' after the function name to get the gas estimation for this particular function 
        return instance.giveAwayDividend.estimateGas(1);

    }).then(function(result) {
        var gas = Number(result);

        console.log("gas estimation = " + gas + " units");
        console.log("gas cost estimation = " + (gas * gasPrice) + " wei");
        console.log("gas cost estimation = " + utils.fromWei((gas * gasPrice), 'ether') + " ether");
    });
});


Gas Price is 20000000000 wei
gas estimation = 26794 units
gas cost estimation = 535880000000000 wei
gas cost estimation = 0.00053588 ether
```




#### Estimate gas vs Estimate costs


- EstimateGas 
   Is present for all calls that change state and only give the computation cost "Gas".

- EstimateCosts
   Only used for ERC20 methods of PublicMint, is a custom function not present in web3 lib that give all info about withdrawals cost.

Example: 
```js
/**
* Return Object 
* {
* gas: estimateGas for current method
* gasPrice: current blockchain gasPrice
* additionalFee: additional cost in tokens value for each method
* totalCost: (estimateGas * gasPrice ) + additionalFee
* }
*/

await USD.withdrawWireInt(value, ref).estimateCosts();

// output
/*
{ gas: '26108',
  gasPrice: '20000000000',
  additionalFee: '50000000000000000000',
  totalCost: '50000522160000000000' 
}
*/

```

[See more]{@tutorial Withdrawals} of Withdrawal costs


