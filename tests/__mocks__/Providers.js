module.exports = [{
        chainId: 2020,
        name: 'mainNet',
        http: 'https://rpc.publicmint.io:8545/',
        ws: 'wss://rpc.publicmint.io:8546/',
        explorer: 'https://explorer.publicmint.io/'
    },
    {
        chainId: 2019,
        name: 'testNet',
        http: 'https://rpc.tst.publicmint.io:8545/',
        ws: 'wss://rpc.tst.publicmint.io:8546/',
        explorer: 'https://explorer.tst.publicmint.io/'
    },
    {
        chainId: 2018,
        name: 'devNet',
        http: 'https://rpc.dev.publicmint.io:8545/',
        ws: 'wss://rpc.dev.publicmint.io:8546/',
        explorer: 'https://explorer.dev.publicmint.io/'
    },
    {
        chainId: 5777,
        name: 'ganache',
        http: 'http://127.0.0.1:7545/',
        ws: 'ws://127.0.0.1:7545/',
        explorer: 'http://127.0.0.1/'
    },
    {
        chainId: 9999,
        name: 'localNet',
        http: 'http://localhost:7545/',
        ws: 'ws://localhost:7545/',
        explorer: 'http://localhost/'
    },

]
