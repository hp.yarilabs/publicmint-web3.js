const ganacheCli = require("ganache-cli").provider();

jest.mock("./PublicMintTestProvider", () => {
    function provider() {
        return ganacheCli;
    }
    return {
        provider
    };
});
