const PublicMint = require('../../src/index.js').default;
const mockAccounts = require('../utils/Accounts');

describe('Wallet', () => {
    let web3;
    let pm;

    beforeAll(done => {
        web3 = new PublicMint();
        pm = web3.pm;
        done();
    });

    it('Should have importFromSeed function', () => {
        expect(pm.wallet).toEqual(
            expect.objectContaining({
                importFromSeed: expect.any(Function),
            })
        );
    });

    it('Should import 2 accounts from seed', () => {
        const mnemonic = 'cook blur blur frown food potato again grain unfold level stage connect';

        const privateKeys = pm.wallet.importFromSeed(mnemonic, 0, 2);
        const expectedPrivKeys = mockAccounts.map(acc => acc.privateKey);
        const expectedPubKeys = mockAccounts.map(acc => acc.address);

        expect(privateKeys).toEqual(expectedPrivKeys);

        expect(
            web3.pm.wallet.accounts.getAccounts
        ).toEqual(expectedPubKeys)
    });

});
