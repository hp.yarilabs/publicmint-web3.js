const PublicMint = require('../../src/index.js').default;


test("PublicMint namespace exist inside web3 module", () => {
    const web3 = new PublicMint();
    expect(web3).toHaveProperty("pm");
})
