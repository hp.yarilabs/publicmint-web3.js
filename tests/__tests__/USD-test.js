/* eslint-disable func-names */
const PublicMint = require('../../src/index.js').default;
const mockAccounts = require('../utils/Accounts')
const fakeBlockchain = require('../utils/FakeBlockchain')


describe('USD', () => {
    let web3;
    let pm;
    let USD;
    let adminUSD;
    let accounts;
    let blockchain;
    let BigNumber;
    const [
        account1,
        account2
    ] = mockAccounts;

    beforeAll(done => {
        return fakeBlockchain().then(function (bc) {
            blockchain = bc;
            accounts = Object.keys(blockchain.provider.manager.state.accounts);

            web3 = new PublicMint(blockchain.ganacheProvider.options.network_id, 'http');
            BigNumber = web3.utils.BN;
            // eslint-disable-next-line prefer-destructuring
            pm = web3.pm;

            // change gnosis address and USD token
            pm.contracts.token.USD.options.address = '0x288dCcF9e6A7Cc571409688388652363E02f16AB';
            pm.contracts.admin.USD.options.address = '0xE1778Ba6B526138598457e2ec847885dC14DdA6e';
            // eslint-disable-next-line prefer-destructuring
            USD = pm.contracts.token.USD;
            adminUSD = pm.contracts.admin.USD;
            done();
        });
    }, 30000);

    afterAll(done => {
        return blockchain.exit(done);
    }, 30000);


    it('Should have accounts getBalances', async () => {
        pm.wallet.add(account2.privateKey);
        pm.wallet.add(account1.privateKey);

        const myBalances = await web3.pm.wallet.accounts.getBalances;
        // balances from state of initial database
        expect(myBalances).not.toEqual({
            '0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd': '0',
            '0xd2FA48924906e00069E81aaf778c4f586b5CA58B': '0'
        })
    });

    it('Should get ERC20 balanceOf', async () => {
        const res = await USD.balanceOf('0x4389Af2E0515dDFe3453B1bD748aDfD5e2598cFd').call();
        expect(res.toString()).toEqual('0')
    });


});
