export default {
  chainId: 2018,
  name: 'devNet',
  http: 'https://rpc.dev.publicmint.io:8545/',
  ws: 'wss://rpc.dev.publicmint.io:8546/',
  explorer: 'https://explorer.dev.publicmint.io/'
};
//# sourceMappingURL=devNet.js.map