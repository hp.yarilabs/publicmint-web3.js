export default {
  chainId: 9999,
  name: 'localNet',
  http: 'http://localhost:7545/',
  ws: 'ws://localhost:7545/',
  explorer: 'http://localhost/'
};
//# sourceMappingURL=localNet.js.map