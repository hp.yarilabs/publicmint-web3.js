/**
 * Providers module.
 * @module Providers
 * @see ProviderConfigurationFiles
 */
import Web3 from 'web3';
import isNumber from 'lodash/isNumber';
import Provider from './Provider';
import networkConfig from './config';
/**
 * Get providers list
 * @return {Provider}
 */

const getProviderList = () => {
  return networkConfig;
};
/**
 * Resolves provider by chain id
 * @private
 * @param  {number} chainId Chain identifier (Defaults to chainId 1 representing mainNet)
 * @return {Provider}
 *          Endpoints for public node connection
 */


const resolveProviderByChainId = (chainId = 1) => {
  const id = chainId && typeof chainId.valueOf() === 'number' ? chainId : parseInt(chainId, 10);
  return getProviderList().filter(provider => provider.chainId === id)[0];
};
/**
 * Resolves provider by network name
 * @private
 * @param {string} networkName  ['mainNet', 'testNet', 'localNet', ...]
 * @return {Provider}
 * @example resolveProviderByNetworkName('mainNet')
 */


const resolveProviderByNetworkName = networkName => {
  return getProviderList().filter(provider => provider.name === networkName)[0];
};
/**
 * Resolves provider given chain id or name of network 
 * @private
 * @param {(string | number)} network 
 * @example `
 * const provider = resolveProvider(1);
 * const provider = resolveProvider('mainNet');
 * `
 */


const resolveProvider = network => {
  if (isNumber(network)) {
    return resolveProviderByChainId(network);
  }

  return resolveProviderByNetworkName(network);
};
/**
 * Create web3 provider from Provider object
 * @private
 * @param {*} defaultProvider 
 * @param {*} selectedProvider 
 */


const loadProviderWeb3 = (defaultProvider = 'http', selectedProvider = getProviderList()[0]) => {
  const providerURL = selectedProvider[defaultProvider];

  if (defaultProvider === 'ws') {
    return new Web3.providers.WebsocketProvider(providerURL);
  }

  return new Web3.providers.HttpProvider(providerURL);
};

export { resolveProviderByChainId, resolveProviderByNetworkName, resolveProvider, loadProviderWeb3, getProviderList, Provider };
export default Provider;
//# sourceMappingURL=index.js.map