/**
 * Query module.
 * @module Query
 * @memberof pm.query
 */
import isObject from 'lodash/isObject';
import isString from 'lodash/isString';
import find from 'lodash/find';
import filter from 'lodash/filter';
import has from 'lodash/has';
/**
 * Find contract address in tokens contracts
 * @private
 * @param { pm.contracts.token } token- List of tokens  
 * @param { string } address - Contract address in string format to search 
 */

const _findAddressInTokens = (tokens, address) => {
  const _address = address.toLowerCase();

  return find(tokens, token => {
    return token.options.address.toLowerCase() === _address;
  });
};
/**
 * Find match in json interface by signature
 * @private
 * @param {*} interfaces 
 * @param {*} input 
 */


const _matchInterfaceSignature = (interfaces, input) => {
  const signature = input.slice(0, 10);
  return find(interfaces, ['signature', signature]);
};
/**
 * Filter all methods that start with 'withdraw' in contract instance
 * @private
 * @param {*} contract 
 */


const _getWithdrawalsInterface = contract => {
  return filter(contract.options.jsonInterface, iface => {
    return iface.hasOwnProperty('name') && iface.name.startsWith("withdraw");
  });
};
/**
 * Given a contract instance and transactionHash resolves and decoded the input.
 * @private
 * @param {*} eth web3-eth with correct connection for blockchain
 * @param {*} contract web3-eth-contract instance
 * @param {*} transactionHash TransactionHash in string format
 * @throws { InvalidContractException | TransactionException } Throws when invalid parsing of transaction or contract abi
 */


const _resolveTransactionByInterface = async (eth, contract, transactionHash) => {
  let transaction;
  transaction = await eth.getTransaction(transactionHash);

  if (!transaction) {
    throw new TransactionException('Transaction not found');
  }

  const interfaces = _getWithdrawalsInterface(contract);

  if (interfaces.length === 0) {
    throw new InvalidContractException('Contract interface or withdrawals interface not exist');
  }

  if (!transaction.hasOwnProperty('input')) {
    throw new TransactionException('This transaction not have parameter input valid');
  }

  const matchedInterface = _matchInterfaceSignature(interfaces, transaction.input);

  if (matchedInterface === undefined) {
    throw new InvalidContractException('Transaction input is invalid, check if is transaction of this contract or withdrawal transaction.');
  }

  const type = matchedInterface.name;
  const decodedValue = eth.abi.decodeParameters(matchedInterface.inputs, '0x' + transaction.input.slice(10));
  return Promise.resolve({
    type,
    decodedValue,
    transaction,
    abi: matchedInterface
  });
};
/**
 * Validate parameters and resolve transaction
 * @private
 * @param {*} web3In web3 instance with provider
 * @param {*} contract web3-eth-contract instance or string address
 * @param {*} transactionHash - TransactionHash to be queried
 * @throws { InvalidArgumentException } - Invalid arguments
 */


const _getTransactionByContract = async (web3In, contract, transactionHash) => {
  // check if is valid transactionHash
  if (!web3In.pm.utils.isTransactionHash(transactionHash)) {
    throw new InvalidArgumentException("Invalid transaction hash");
  } // check if is an address


  if (isString(contract)) {
    if (!web3In.utils.isAddress(contract)) {
      throw new InvalidArgumentException("Invalid contract address");
    } // search in all contracts 


    const tokenInterface = _findAddressInTokens(web3In.pm.contracts.token, contract);

    return _resolveTransactionByInterface(web3In.eth, tokenInterface, transactionHash);
  } // check if is web3-contracts


  if (isObject(contract)) {
    if (!contract.hasOwnProperty('methods') && !has(contract.methods, ['options', 'options.jsonInterface'])) {
      throw new InvalidArgumentException("Invalid contract instance, not have 'options' and jsonInterface");
    }

    return _resolveTransactionByInterface(web3In.eth, contract, transactionHash);
  }
};
/**
 * Invalid params exception
 * @param {string} message Error message
 */


function InvalidArgumentException(message) {
  this.message = message;
  this.name = 'InvalidArgumentException';
}
/**
 * Invalid contract exception
 * @param {string} message Error message
 */


function InvalidContractException(message) {
  this.message = message;
  this.name = 'InvalidContractException';
}
/**
 * Transaction exception
 * @param {string} message Error message
 */


function TransactionException(message) {
  this.message = message;
  this.name = 'TransactionException';
}

export default function (web3In) {
  return {
    /** 
     * getWithdrawalTransactionByContract
     * Resolve if transaction hash belongs to contract
     * @public
     * @param { string | web3.eth.contract } - Contract instance or address in string format
     * @param { string } - TransactionHash in hex string format 
     * @return { Object } - Result object
     * @throws { InvalidArgumentException | InvalidContractException }
     */
    getWithdrawalTxByContract: (contract, hash) => {
      return _getTransactionByContract(web3In, contract, hash);
    },

    /** 
     * getWithdrawalByTransactionInContract
     * Resolve if transaction hash belongs to contract
     * @public
     * @param { string | web3.eth.contract } - Contract instance or address in string format
     * @param { string } - TransactionHash in hex string format 
     * @return { string } - Result withdrawal type
     * @throws { InvalidArgumentException | InvalidContractException }
     */
    getWithdrawalTypeByContract: (contract, hash) => {
      return _getTransactionByContract(web3In, contract, hash).then(returnedValue => {
        return Promise.resolve(returnedValue.type);
      });
    }
  };
}
//# sourceMappingURL=index.js.map