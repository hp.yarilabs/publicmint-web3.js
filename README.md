<p align="center">
<img src="https://gitlab.com/public-mint-community/publicmint-web3.js/-/raw/master/assets/pm-web3.png" width=200 />
</p>

# PublicMint library for javascript 

Public Mint client library built around web3.js customized for blockchain interaction.

This package creates a new namespace pm `Public Mint` inside web3 with custom contracts instances, functions and networks configurations.

### Documentation

- [PublicMint-web3](https://public-mint.gitlab.io/publicmint-web3.js/)


## Dev instructions

### How to install 

```bash
npm install @publicmint/publicmint-web3
```

###  How to use
- [Quick start tutorial](https://public-mint-community.gitlab.io/publicmint-web3.js/tutorial-QuickStart.html)

### Removed modules from web3

- eth.personal (PublicMint blockchain client not have account management)
- eth.ens (Web3 incompatibility to change provider waiting for [PR 3010](https://github.com/ethereum/web3.js/issues/3010)) 
- bzz (Needs revision / implementation)
- shh (Needs revision / implementation)

### Build doc

```bash 
npm run build:doc
```

