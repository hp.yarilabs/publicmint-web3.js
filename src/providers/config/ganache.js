export default {
    chainId: 5777,
    name: 'ganache',
    http: 'http://127.0.0.1:7545/',
    ws: 'ws://127.0.0.1:7545/',
    explorer: 'http://127.0.0.1/'
}
