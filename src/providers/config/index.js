/**
 * Module exposing all configurations for networks
 * @module ProviderConfigurationFiles
 * @namespace ProviderConfigurationFiles
 * @see ProviderConfiguration
 * @example
 * `
 * Configurations exposed :
 * 
 * |----------|------------------------------------------------------|
 * | Chain Id | Description                                          |
 * |----------|------------------------------------------------------|
 * | mainNet  | Main network                                         |
 * | testNet  | Test network                                         |
 * | devNet   | Client development network (PublicMint internal use) |
 * | localNet | Local network                                        |
 * | ganache  | Local contract development network                   |
 * `
 */

import mainNet from './mainNet';
import testNet from './testNet';
import devNet from './devNet';
import localNet from './localNet';
import ganache from './ganache';


export default [
    mainNet,
    testNet,
    devNet,
    localNet,
    ganache
]
