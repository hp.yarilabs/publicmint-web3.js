/**
 * ERC20 contracts configuration file 
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Token symbol (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name: 
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 */
export default [{
    "name": "GasManager",
    "abi": "GasManager",
    "deployType": "genesis",
    "deployInfo": [{
        "address": "0x0000000000000000000000000000000000004070"
    }]
}]
