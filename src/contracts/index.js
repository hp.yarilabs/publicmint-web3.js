/**
 * PublicMint contracts interface
 * This module resolves configurations of contracts and create interface in `web3.pm.contracts`
 * @example `const { USD } = web3.pm.contracts.token`;
 * @module Contracts
 */

import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import flatten from 'lodash/flatten';
import reduce from 'lodash/reduce';
import upperFirst from 'lodash/upperFirst';
import find from 'lodash/find';


import tokensConfig from './tokens/config';
import adminConfig from './admin/config';
import operationsConfig from './operations/config';
import BaseContract from './web3-lib/contract/index';

// CONTRACTS ABI
import ERC20 from './tokens/abi/ERC20.json';
import GNOSIS from './admin/abi/GNOSIS.json';
import GasManager from './operations/abi/GasManager.json';
import GasManagerPseudoEvents from './operations/abi/GasManagerPseudoEvents.json';

/**
 * Address exception - address invalid or null
 * @param {string} message Error message
 */
function InvalidAddressException(message) {
  this.message = message;
  this.name = 'InvalidAddressException';
}

/**
 * ABI exception - invalid or undefined
 * @param {string} message Error message
 */
function InvalidAbiException(message) {
  this.message = message;
  this.name = 'InvalidAbiException';
}

/**
 * Load generic contracts scheme with custom abi
 * @access private
 * @param {*} contractConfig
 * @param {*} isAddress
 * @param {*} chainId
 * @param {*} abi
 */
const loadGenericContractScheme = (contractConfig, isAddress, chainId, abiList) => {
  return contractConfig.map(config => {
    let targetAddress;
    if (config.deployType === 'genesis') {
      targetAddress = config.deployInfo[0].address;
    } else {
      targetAddress = config.deployInfo.filter(add => add.chainId === chainId)[0].address;
    }

    if (!targetAddress || !isAddress(targetAddress))
      throw new InvalidAddressException('Invalid address in contract configuration file');

    let abi

    if (Array.isArray(config.abi)) {
      abi = config.abi
    } else {
      abi = find(abiList, (abi) => {
        return abi.name === config.abi
      });

      abi = abi ? abi.data : null
    }

    if (!abi) {
      throw new InvalidAbiException('Contract ABI not found or invalid');
    }

    return Object.assign(config, {
      address: targetAddress,
      abi: abi
    });
  });
};
/**
 * Group all contracts configuration to create namespace object
 * @access private
 * @param  {...any} args
 */
const loadConfigs = (isAddress, chainId) => {
  // admin needs to load after all contracts
  const configs = [{
      name: 'operations',
      namespaceConfig: operationsConfig
    },
    {
      name: 'token',
      namespaceConfig: tokensConfig
    }, {
      name: 'admin',
      namespaceConfig: adminConfig
    },
  ];

  const contractsABI = [{
    name: "GasManager",
    data: [...GasManager, ...GasManagerPseudoEvents]
  }, {
    name: "ERC20",
    data: ERC20,
  }, {
    name: "GNOSIS",
    data: GNOSIS
  }];

  return configs.map((cfg) => {
    return Object.assign(cfg, {
      contracts: loadGenericContractScheme(cfg.namespaceConfig, isAddress, chainId, contractsABI)
    });
  });
}

/**
 * This will wrap multi sig `submitTransaction` for target contract like USD
 * This will prefix all methods that contract admins transforming the first
 * letter in uppercase and add a prefix with `proxy` word.
 * Using `web3.pm.contracts.admin.proxyDeposit(<depositArgs>)`
 * @access private
 * @throws - All web3 errors and exceptions
 * @param {*} contracts all created contracts before
 * @param {*} adminsTo property inside current contract
 * @param {*} currentContract current contract methods
 * @return {Array.<Object>}
 * @example `returns [{ deposit: [Function: deposit] } ]`
 */
const resolveAdminMethods = (contracts, {
  adminsTo
}, currentContract) => {
  if (adminsTo) {
    const listOfMethodsResolved = flatten(
      adminsTo.map(adminContract => {
        const adminMethods = get(contracts, adminContract.target, 'null');
        return Object.keys(adminMethods)
          .filter(key => adminContract.proxyMethods.includes(key))
          .map(methodKey => {
            return {
              [`proxy${upperFirst(methodKey)}`]: (...args) => {
                const target = adminMethods.options.address;
                let encodedData = null;
                try {
                  encodedData = adminMethods[methodKey](...args).encodeABI();
                } catch (error) {
                  return error;
                }
                return currentContract.submitTransaction(target, 0, encodedData);
              }
            };
          });
      })
    );

    return reduce(listOfMethodsResolved, (methods, current) => {
      return Object.assign(methods, current);
    });
  }
  return null;
};


/**
 * Should create a interface of contracts in configuration file,
 * passing the methods directly to root (we can use web3 style using prefix of `methods`) of object with name given.
 * @private
 * @param {*} web3in web3 instance
 * @param {*} chainId selected chain id
 * @param {*} opts custom contract options
 */
const createContractsInterface = (web3in, chainId, opts = {}) => {
  const {
    isAddress
  } = web3in.utils;

  const namespaceConfigs = loadConfigs(isAddress, chainId);
  const contracts = {};
  // loop all namespaces configs
  namespaceConfigs.forEach(namespaceConfig => {
    // load all contracts for parent namespace
    return namespaceConfig.contracts.forEach(co => {

      const ContextContract = BaseContract.bind(web3in.eth)
      BaseContract.setProvider(web3in.currentProvider, web3in.eth.accounts);

      const instance = new ContextContract(co.abi, co.address, opts);
      let {
        methods
      } = instance;

      // load proxy admin methods for multiSig wallets
      const adminMethods = resolveAdminMethods(contracts, co, methods);

      if (!isEmpty(adminMethods)) {
        methods = Object.assign(methods, adminMethods);
      }

      const contractsNamespace = contracts[namespaceConfig.name];
      const contractsNamespaceValue = !contractsNamespace ? {} : contractsNamespace;

      contracts[namespaceConfig.name] = Object.assign(contractsNamespaceValue, {
        [co.name]: {
          ...instance,
          ...methods
        }
      });
    });
  });
  return contracts;
};

export default createContractsInterface;
