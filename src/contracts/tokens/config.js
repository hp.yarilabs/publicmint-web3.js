/**
 * ERC20 contracts configuration file 
 * @namespace ContractsConfiguration 
 * @property {string}  name          - Token symbol (must be unique).
 * @property {string}  deployType    - Type of deploy: 
 *  - `genesis` load one address
 *  - `manual`  load config by chain id.
 * @property {string}  abi    - ABI name: 
 * @property {Array.<Object.<chainId, address>>}  deployInfo - Info with address and chainId
 */
export default [{
    "name": "USD",
    "deployType": "genesis",
    "abi": "ERC20",
    "deployInfo": [{
        "address": "0x0000000000000000000000000000000000002070"
    }]
}]
