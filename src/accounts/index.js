/**
 * PublicMint Wallet accounts.
 * - Get all balance(s) of wallet
 * - Transfer tokens
 * @namespace Accounts
 */

import pickBy from 'lodash/pickBy';
import range from 'lodash/range';
import values from 'lodash/values';
import zipObject from 'lodash/zipObject';
import {
	mnemonicToSeedSync
} from 'bip39';
import {
	fromMasterSeed
} from 'hdkey';
/**
 * Get Addresses from wallet
 * @memberof Accounts
 */
const getAccounts = wallet => {
	const n = wallet.length;
	if (n === 0) return null;

	const indexAccounts = range(n).map(index => index.toString());

	return values(
		pickBy(wallet, (_value, key) => {
			return indexAccounts.includes(key);
		})
	).map(accounts => accounts.address);
};

/**
 * Get all balances in wallet
 * @memberof wallet.accounts
 * @return Promise
 * @throws {error} In case of fail connection or empty wallet
 * @memberof Accounts
 */
const getBalances = async (eth, addresses) => {
	return new Promise((resolve, reject) => {
		if (addresses !== null && addresses.length !== 0) {
			Promise.all(addresses.map(address => eth.getBalance(address)))
				.then(balances => {
					resolve(zipObject(addresses, balances), (address, balance) => {
						return {
							address,
							balance
						};
					});
				})
				.catch(errors => {
					reject(errors);
				});
		} else {
			reject(new Error('Wallet is empty'));
		}
	});
};

/**
 * Transfer tokens from accounts in wallet to other account
 * @private
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */
const pTransfer = (eth, to, value, options) => {
	// TODO: change default gas
	const transactionObject = {
		to,
		value,
		gas: '50000',
		...options
	};

	return eth.sendTransaction(transactionObject);
};

/**
 * Transfer tokens from accounts in wallet to other account
 * By default send from first account in wallet
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */
function transfer(to, value, options = {}) {
	const newOptions = {
		from: 0,
		...options
	};
	return pTransfer(this, to, value, newOptions);
}

/**
 * Transfer tokens from accounts in wallet to other account
 * @param {number | string} from wallet address (index of account or address)
 * @param {string} to Recipient address
 * @param {string} value Amount of tokens to transfer
 * @param {*} options Overwrite defaults options [TransactionObject](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#signtransaction)
 * @memberof Accounts
 */
function transferFrom(from, to, value, options = {}) {
	const newOptions = {
		from,
		...options
	};
	return pTransfer(this, to, value, newOptions);
}

const accountHandler = {
	get: (eth, key) => {
		const {
			wallet
		} = eth.accounts;
		let result = wallet;
		switch (key) {
			case 'getAccounts':
				result = getAccounts(wallet);
				break;
			case 'getBalances':
				result = getBalances(eth, getAccounts(wallet));
				break;

			case 'getBalance':
				/**
				 * @function getBalance
				 * @memberof Accounts
				 * @param { String } - Account address
				 * @return { Promise } - The current balance for the given address in wei.
				 */
				result = address => {
					return eth.getBalance(address);
				};
				break;
			case 'transfer':
				result = (...args) => {
					return transfer.call(eth, ...args);
				};
				break;
			case 'transferFrom':
				result = (...args) => {
					return transferFrom.call(eth, ...args);
				};
				break;
			default:
				break;
		}
		return result;
	}
};

function init(eth) {
	return new Proxy(eth, accountHandler);
}

/**
 * importFromSeed - 
 * Import from seed to wallet using bip39 algorithm with ethereum compatible derived path
 * @public
 * @memberof Accounts
 * @param {string} seed  - Mnemonic seed
 * @param {number} [start=0] - Start index
 * @param {number} [total=1] - Total number of addresses to load
 */
function importFromSeed(seed, start = 0, total = 1) {
	let seedBuffer = mnemonicToSeedSync(seed);
	let hdWallet = fromMasterSeed(seedBuffer);
	let privateKeys = [];

	for (let i = start; i < start + total; i++) {
		const privateKeyBuffer = hdWallet.derive("m/44'/60'/0'/0/" + i)._privateKey;
		const pkHex = '0x' + Buffer.from(privateKeyBuffer).toString('hex');
		privateKeys.push(pkHex);
		this.eth.accounts.wallet.add(pkHex);
	}
	return privateKeys;
}

const acc = function (web3In) {
	const {
		wallet
	} = web3In.eth.accounts;

	wallet.accounts = init(web3In.eth);
	wallet.importFromSeed = importFromSeed.bind(web3In);

	return wallet;
};

export default acc;
