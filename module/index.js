import 'core-js';
import Web3 from 'web3';
import accounts from './accounts';
import createContractsInterface from './contracts';
import { resolveProvider, getProviderList, loadProviderWeb3, Provider } from './providers';
import { version } from '../package.json';
import utils from './utils';
import query from './query';
/**
 * Provider exception
 * @param {string} message Error message
 */

function InvalidProviderException(message) {
  this.message = message;
  this.name = 'InvalidProviderException';
}

class PublicMint {
  /**
   * PublicMint - javascript devKit
   * Adds new namespace inside web3 `web3.pm`
   * This module depends of web3 package readme more [here](https://web3js.readthedocs.io/en/v1.2.4/index.html)
   *
   * @constructor
   * @param { Number | String | Provider } [ provider = 1 | provider = 'mainNet' | provider = new Provider ] provider
   * @param { String } [ connectionType = 'ws' ] connectionType ('ws' || 'http' ) default setting is ws (events).
   * @param { Object } [ customWeb3Options = { defaultBlock: 'latest', transactionConfirmationBlocks: 1 } ] - Web3 options example: [web3 docs](https://web3js.readthedocs.io/en/v1.2.4/web3-eth.html#transactionconfirmationblocks)
   * @param { Object } [ customContractsOptions = {  } ] - Contract options example: [web3 docs](https://web3js.readthedocs.io/en/v1.2.4/web3-eth-contract.html#web3-eth-contract)
   *
   * @return { Object } Will return an object with the classes of all major sub module from web3, including PublicMint modules
   *
   * [QuickStart tutorial]{@tutorial QuickStart}
   * @example const web3 = new PublicMint();
   */
  constructor(provider = 2020, connectionType = 'ws', customWeb3Options = {}, customContractsOptions = {}) {
    const web3opts = {
      defaultBlock: 'latest',
      transactionConfirmationBlocks: 1,
      ...customWeb3Options
    };
    this.provider = provider instanceof Object ? provider : resolveProvider(provider);

    if (this.provider === undefined) {
      throw new InvalidProviderException(`Provider identifier ${provider} not found`);
    }

    this.loadedWeb3Provider = loadProviderWeb3(connectionType, this.provider);
    this.web3 = new Web3(this.loadedWeb3Provider, null, web3opts);
    const contracts = createContractsInterface(this.web3, this.provider.chainId, customContractsOptions);
    /**
     *  @namespace pm
     *  @property { Object } pm.contracts - Contracts instances
     *  @property { Array<Object> } pm.providers - List of providers
     *  @property { Provider } pm.provider Provider in use
     *  @property { Accounts } pm.wallet - Module for manage accounts and related methods (sign, transfer, ...)
     *  @property { Utils } pm.utils - [Utils module ](Utils)
     *  @property { String }  pm.version - Module Version
     *  @property { Query } pm.query - Module with helper functions for query blockchain.
     */

    this.web3 = Object.defineProperty(this.web3, 'pm', {
      value: {
        /**
         *  Contracts instances
         * @memberof pm
         * @type { Object }
         * @namespace pm.contracts
         * @property { Object } contracts.USD - ERC20  contract instance for USD token
         * @property { Object } contracts.admin.USD - Multi Signature contract instance for USD
         */
        contracts,

        /**
         *  Contracts instances
         * @memberof pm
         * @type { Object }
         * @namespace pm.providers
         * @property { Function } providers.getProviderList  - Get list of providers
         */
        providers: getProviderList,

        /**
         *  Contracts instances
         * @memberof pm
         * @type { Provider }
         * @namespace pm.provider
         * @property { Function } provider - Instance provider
         */
        provider: this.provider,

        /**
         *  Module of wallet actions [more info](https://web3js.readthedocs.io/en/v1.2.0/web3-eth-accounts.html#wallet)
         * @memberof pm
         * @type { Object }
         * @namespace pm.wallet
         * @property { Function } wallet.add - Adds an account using a private key or account object to the wallet.
         * @property { Function } wallet.remove - Removes an account from the wallet.
         * @property { Function } wallet.load - Loads a wallet from local storage and decrypts it.
         * @property { Function } wallet.clear - Securely empties the wallet and removes all its accounts.
         * @property { Function } wallet.encrypt - Encrypts all wallet accounts to an array of encrypted keystore v3 objects.
         * @property { Function } wallet.decrypt - Decrypts keystore v3 objects.
         * @property { Function } wallet.save - Stores the wallet encrypted and as string in local storage.
         * @property { Function } wallet.load - Loads a wallet from local storage and decrypts it.
         * @property { Object } wallet.accounts - Actions for added accounts.
         * @property { Function } wallet.accounts.getBalance - Get balance of account  `accounts.getBalance('0x0')`.
         * @property { Function } wallet.accounts.getBalances - Get balance of account `accounts.getBalances`.
         * @property { Function } wallet.accounts.transfer - Transfer balance using first wallet account.
         * @property { Function } wallet.accounts.transferFrom - TransferFrom same as transfer but specifying index or address .
         */
        wallet: accounts(this.web3),

        /**
         *  Module of utils
         * @memberof pm
         * @type { Object }
         * @namespace utils
         */
        utils: utils(this.web3.utils),

        /**
         *  Version of module
         * @memberof pm
         * @type { String }
         */
        version,

        /**
         *  Version of module
         * @memberof pm
         * @type { Query }
         */
        query: query(this.web3)
      }
    }); // Delete modules not compatible with client
    // TODO: needs revision later

    delete this.web3.eth.personal;
    delete this.web3.eth.ens;
    delete this.web3.bzz;
    delete this.web3.shh;
    return this.web3;
  }
  /**
   * Provider constructor
   */


  static provider(...args) {
    return new Provider(...args);
  }
  /**
   * Get providers list
   */


  static providers() {
    return getProviderList();
  }
  /**
   * Modules versions
   */


  static versions() {
    return {
      Web3: Web3.version,
      PublicMint: version
    };
  }

} // Expose utils


PublicMint.utils = { ...Web3.utils,
  ...utils(Web3.utils)
};
export default PublicMint;
//# sourceMappingURL=index.js.map